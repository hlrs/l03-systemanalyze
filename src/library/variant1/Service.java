package library.variant1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public interface Service {
    Book addBook(String author, String title, String ISBN);

    Book[] searchBooks(String author, String title, String ISBN);

    void issueBook(Book book);

    void returnBook(Book book);

    Book[] getIssuedBooks(User user);
}
