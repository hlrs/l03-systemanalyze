package library.variant1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class ServiceImplementation implements Service {
    private Book[] books;

    @Override
    public Book addBook(String author, String title, String ISBN) {
        return null;
    }

    @Override
    public Book[] searchBooks(String author, String title, String ISBN) {
        return new Book[0];
    }

    @Override
    public void issueBook(Book book) {

    }

    @Override
    public void returnBook(Book book) {

    }

    @Override
    public Book[] getIssuedBooks(User user) {
        return new Book[0];
    }
}
