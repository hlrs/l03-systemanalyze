package library.variant1;

import java.util.Date;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class Notification {
    private Book book;
    private User user;
    private Date returnDate;
}
