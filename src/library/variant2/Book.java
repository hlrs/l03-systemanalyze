package library.variant2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class Book {
    private String author;
    private String title;
    private String ISBN;

    /**
     * The count of book instances in library.
     * For example, library can have two instances of "Alexandre Dumas - The Three Musketeers". So there will be only
     * one instance of class Book where count = 2.
     */
    private int count;

    public Book(String author, String title, String ISBN) {
        this.author = author;
        this.title = title;
        this.ISBN = ISBN;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
