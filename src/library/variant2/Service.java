package library.variant2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public interface Service {
    Book addBook(String author, String title, String ISBN);

    Book[] searchBooks(String author, String title, String ISBN);

    void issueBook(User user, Book book);

    void returnBook(User user, Book book);

    Book[] getIssuedBooks(User user);
}
