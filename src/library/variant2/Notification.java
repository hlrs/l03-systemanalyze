package library.variant2;

import java.util.Date;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class Notification {
    private Book book;
    private User user;
    private Date returnDate;

    public Notification(Book book, User user, Date returnDate) {
        this.book = book;
        this.user = user;
        this.returnDate = returnDate;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }
}
