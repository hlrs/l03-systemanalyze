package library.variant2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class Admin {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
