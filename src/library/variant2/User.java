package library.variant2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class User {
    private String login;
    private String password;
    private Book[] issuedBooks;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Book[] getIssuedBooks() {
        return issuedBooks;
    }

    public void setIssuedBooks(Book[] issuedBooks) {
        this.issuedBooks = issuedBooks;
    }
}
