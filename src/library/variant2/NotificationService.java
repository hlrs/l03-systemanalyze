package library.variant2;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class NotificationService {
    /**
     * The delay between issuing a book and sending notification (in case user did't return book during this period).
     */
    private static final long RETURN_BOOK_NOTIFICATION_DELAY = 3 * 7 * 24 * 60 * 60 * 1000L;  //3 weeks

    private Notification[] notifications;

    /**
     * Util class which helps to work with timers.
     */
    private Timer timer;
    /**
     * Timers itself.
     */
    private Task[] tasks;

    public NotificationService() {
        notifications = new Notification[0];

        timer = new Timer(true);
        tasks = new Task[0];
    }

    /**
     * Notify NotificationService the book is issued by library to user.
     */
    public void issueBook(User user, Book book) {
        //extend tasks array capacity to add new timer
        Task[] newTasks = new Task[tasks.length + 1];
        System.arraycopy(tasks, 0, newTasks, 0, tasks.length);
        tasks = newTasks;

        //create new timer
        Date returnDate = new Date(System.currentTimeMillis() + RETURN_BOOK_NOTIFICATION_DELAY);
        Task task = new Task(book, user, returnDate);
        tasks[tasks.length - 1] = task;

        //start new timer
        timer.schedule(task, returnDate);
    }

    /**
     * Notify NotificationService the user returns issued book to library.
     */
    public void returnBook(User user, Book book) {
        //search for appropriate timer
        int newLength = tasks.length - 1;
        Task[] newTasks = new Task[newLength];

        int taskIndex = 0;
        for (int i = 0; i < tasks.length; i++) {
            Task task = tasks[i];

            if (book.equals(task.book) && user.equals(task.user)) {
                taskIndex = i;
                //when found, stop this timer (we don't need notification)
                task.cancel();
                break;
            }
        }

        //reduce tasks array capacity and remove timer
        System.arraycopy(tasks, 0, newTasks, 0, taskIndex);
        System.arraycopy(tasks, taskIndex + 1, newTasks, taskIndex, newLength - taskIndex);

        tasks = newTasks;
    }

    private void sendNotification(Notification notification) {
        //todo send to admin
    }

    /**
     * Timer to countdown time for notification
     */
    private class Task extends TimerTask {
        private Book book;
        private User user;
        private Date returnDate;

        public Task(Book book, User user, Date returnDate) {
            this.book = book;
            this.user = user;
            this.returnDate = returnDate;
        }

        /**
         * This method will be invoked when:
         * 1. the countdown of this timer is complete
         * 2. this timer was not canceled by returnBook method
         */
        @Override
        public void run() {
            //create new notification and send it to administrators
            Notification notification = new Notification(book, user, returnDate);
            sendNotification(notification);
        }
    }
}
