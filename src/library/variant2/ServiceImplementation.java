package library.variant2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.06.2017
 */
public class ServiceImplementation implements Service {
    private static final int INITIAL_STORAGE_SIZE = 16;

    private NotificationService notificationService;

    private Book[] books;
    private int bookCount;

    public ServiceImplementation() {
        books = new Book[INITIAL_STORAGE_SIZE];
    }

    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public Book addBook(String author, String title, String ISBN) {
        Book book;

        //firstly make a search - if we already have such book in library
        Book[] existingBooks = searchBooks(author, title, ISBN);
        if (existingBooks.length == 0) {
            //if not - create new instance of Book class and add it to library
            book = new Book(author, title, ISBN);
            addNewBook(book);
        } else {
            //if so - just increase book instance counter in existing instance of Book class
            book = existingBooks[0];
            addNewBookInstance(book);
        }

        return book;
    }

    @Override
    public Book[] searchBooks(String author, String title, String ISBN) {
        //create an temporary array to help us to search for books
        Book[] foundBooks = new Book[books.length];
        int foundCount = 0;

        //go through all books and search for books with specified parameters
        for (Book book : books) {
            boolean foundByAuthor = author == null || book.getAuthor().contains(author);
            boolean foundByTitle = title == null || book.getTitle().contains(title);
            boolean foundByISBN = ISBN == null || book.getISBN().contains(ISBN);

            if (foundByAuthor && foundByTitle && foundByISBN) {
                foundBooks[foundCount] = book;
                foundCount++;
            }
        }

        //create an result array and copy found books to it
        //so the length of result array will be the count of books we have found
        Book[] result = new Book[foundCount];
        System.arraycopy(foundBooks, 0, result, 0, foundCount);
        return result;
    }

    @Override
    public void issueBook(User user, Book book) {
        //extend issuedBooks array to issue new one book to user
        Book[] issuedBooks = user.getIssuedBooks();
        Book[] newIssuedBooks = new Book[issuedBooks.length + 1];

        System.arraycopy(issuedBooks, 0, newIssuedBooks, 0, issuedBooks.length);
        newIssuedBooks[newIssuedBooks.length - 1] = book;

        user.setIssuedBooks(newIssuedBooks);

        //notify NotificationService about this event
        notificationService.issueBook(user, book);
    }

    @Override
    public void returnBook(User user, Book book) {
        //search for previously issued book
        Book[] issuedBooks = user.getIssuedBooks();

        int newLength = issuedBooks.length - 1;
        Book[] newIssuedBooks = new Book[newLength];

        int bookIndex = 0;
        for (int i = 0; i < issuedBooks.length; i++) {
            if (book.equals(issuedBooks[i])) {
                bookIndex = i;
                break;
            }
        }

        //reduce capacity of issuedBooks array and remove returned book from it
        System.arraycopy(issuedBooks, 0, newIssuedBooks, 0, bookIndex);
        System.arraycopy(issuedBooks, bookIndex + 1, newIssuedBooks, bookIndex, newLength - bookIndex);

        user.setIssuedBooks(newIssuedBooks);

        //notify NotificationService about this event
        notificationService.returnBook(user, book);
    }

    @Override
    public Book[] getIssuedBooks(User user) {
        return user.getIssuedBooks();
    }

    private void addNewBook(Book book) {
        //extend books array capacity twice if it's full
        if (bookCount == books.length) {
            Book[] newBooks = new Book[books.length];
            System.arraycopy(books, 0, newBooks, 0, books.length);
            books = newBooks;
        }

        books[bookCount] = book;
        bookCount++;
    }

    private void addNewBookInstance(Book book) {
        int currentBookInstanceCount = book.getCount();
        book.setCount(currentBookInstanceCount + 1);
    }
}
